/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        //this.receivedEvent('deviceready');
        console.log('Received Device Ready Event');
        console.log('calling setup push');
        app.setupPush();
    },

    setupPush: function() {
        
        console.log('calling push init');
        var device_platform=device.platform;
        //alert(device_platform);
        console.log(device_platform)
        localStorage.setItem('device_type', device_platform);
        FCMPlugin.getToken(function(token) {
    //this is the fcm token which can be used
    //to send notification to specific device 
    console.log('registration event: ' + token);
             var oldRegId = localStorage.getItem('registrationId');
            if (oldRegId !== token.registrationId) {
                // Save new registration ID
                localStorage.setItem('registrationId', token);
                localStorage.setItem('device_token', token);
                // Post registrationId to your app server as the value has changed
                
            }
            console.log("registration token from localStorage:"+localStorage.getItem("registrationId"));
            // var parentElement = document.getElementById('registration');
            // var listeningElement = parentElement.querySelector('.waiting');
            // var receivedElement = parentElement.querySelector('.received');

            // listeningElement.setAttribute('style', 'display:none;');
            // receivedElement.setAttribute('style', 'display:block;');
    //FCMPlugin.onNotification( onNotificationCallback(data), successCallback(msg), errorCallback(err) )
    //Here you define your application behaviour based on the notification data.

            var webapp_notifications = [];

            FCMPlugin.onNotification(function(data) {
                console.log("notification entry:::::"+JSON.stringify(data));
                webapp_notifications.push(data);
                localStorage.setItem('my_notifications', JSON.stringify(webapp_notifications));
                console.log(localStorage.getItem('my_notifications'));
               if(data.wasTapped){
                  //Notification was received on device tray and tapped by the user.
                  //alert( JSON.stringify(data) );
                  console.log("//////////Tapped/////////");
                  /*webapp_notifications.push(data);
                  localStorage.setItem('my_notifications', JSON.stringify(webapp_notifications));
                  console.log(localStorage.getItem('my_notifications'));*/
                  /*console.log("///////////////////");
                  console.log(localStorage.getItem('myapp_notifications'));*/
                }else{
                  //Notification was received in foreground. Maybe the user needs to be notified.
                  //alert( JSON.stringify(data) );                 
                  console.log("//////////Not Tapped/////////");
                  /*webapp_notifications.push(data);
                  localStorage.setItem('my_notifications', JSON.stringify(webapp_notifications));
                  console.log(localStorage.getItem('my_notifications'));*/
                  /*console.log("///////////////////");
                  console.log(localStorage.getItem('myapp_notifications'));*/
                }
            });
            FCMPlugin.onTokenRefresh(function(token){
                localStorage.setItem('device_token', token);
            //alert( token );
        });
    });
    },
    
    
                     
    
    // Update DOM on a Received Event
    // receivedEvent: function(id) {
    //     var parentElement = document.getElementById(id);
    //     var listeningElement = parentElement.querySelector('.listening');
    //     var receivedElement = parentElement.querySelector('.received');

    //     listeningElement.setAttribute('style', 'display:none;');
    //     receivedElement.setAttribute('style', 'display:block;');

    //     console.log('Received Event: ' + id);
    // }
};

app.initialize();