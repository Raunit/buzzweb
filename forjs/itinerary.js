
  $(document).ready(function(){
       $("#itinerary1").css({
                   "color":"#c62828",
       });
  });

    
function formatDate_last_regs(d,type)
{
    var day, month, year;
    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var monthNames = [
    "Jan", "Feb", "March",
    "April", "May", "Jun", "July",
    "Aug", "Sept", "Oct",
    "Nov", "Dec"
 	];
    var month_index = ['0','1','2','3','4','5','6','7','8','9','10','11'];
    
    result = d.match("[0-9]{2}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{4}");
    if(null != result) {
        dateSplitted = result[0].split(result[1]);
        day = dateSplitted[0];
        month = dateSplitted[1];
        year = dateSplitted[2];
    }
    result = d.match("[0-9]{4}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{2}");
    if(null != result) {
        dateSplitted = result[0].split(result[1]);
        day = dateSplitted[2];
        month = dateSplitted[1];
        year = dateSplitted[0];
    }
    
    
    if(month>12) {
        aux = day;
        day = month;
        month = aux;
    }
    var monthint;
    var our_request_date_time=d.split(' ');
    var outtime=our_request_date_time[1];
    var ourtime_format=our_request_date_time[2];
    if((monthint=parseInt(month)<10)){
    month = month.substring(1, month.month);
    }
    var ourdate=new Date(monthNames[month-1]+' '+day+' '+year);
    var ourdays=ourdate.getDay();
    
//    return year+"/"+monthNames[month-1]+"/"+day+'  '+result;
    if(type==1){
        return outtime+' '+ourtime_format+', '+days[ourdate.getDay()]+', '+day + ' ' +monthNames[month-1];
    }
    if(type==2){
        return days[ourdate.getDay()]+', '+day + ' ' +monthNames[month-1];
    }
    	
   // return ourdays;
    
}
    $(".dots").click(function(){
     $(".feed-list").slideToggle("fast");
  });
    $('#survey2').show();
    $('#itinerary2').show();
  $(document).ready(function(){
  	//document.getElementById("module").innerHTML="Itinerary";
  	if (!localStorage.getItem("user_id")) {
     window.location.href=window.location.origin+'/'+localStorage.getItem("web_url")+'/index.html';
    }else{
    	var id = JSON.parse(localStorage.getItem("user_id"));
							
		var data_type={"user_id": id};
		//console.log(data_type);
		$.ajax({  

		url: localStorage.getItem("base_url")+'dashboard',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(data_type),
		ContentType: 'application/json',
		type : 'POST',
		dataType : 'json',
		async: false,
		success : function (output) {
			if (output.status==1000){
				console.log(output);
				
				localStorage.setItem("user_data", JSON.stringify(output.data));
			
				localStorage.removeItem("mobile_no");
				localStorage.removeItem("activation_code");

			}else{
				console.log(output.message);
			}
		},
		error : function(){
			alert("Error in loading records. Please refresh the page and try again.");
		}
		}); 
    }

     //alert("Ravi");
      console.log("Ravi");
	  var user_data = JSON.parse(localStorage.getItem("user_data"));
	  console.log(user_data);
	  console.log("//////////");
	  console.log(user_data.itinerary);
	  var itinerary_view = "";
	  var u_id = user_data.user.id;
	  var c_id = user_data.user.company_id;

	  if((user_data.itinerary.status == 1) && (user_data.itinerary.data.length > 0)){
	  for (i = 0; i < user_data.itinerary.data.length; i++) {
          
          
          
        var r_base = user_data.itinerary.data[i].role_base;
          var role_base=[];
          if(r_base!=null){
              role_base=r_base.split(',');
              
          }
         
	  	var u_base = user_data.itinerary.data[i].user_base;
         var user_base=[];
          if(u_base!=null ){
              user_base=u_base.split(',');
              
          }

	  	var u_role = user_data.user.user_role;
	  	var u_base = user_data.user.id;
          

	  	var coordinator_base = user_data.itinerary.data[i].coordinator;
	  	var coordinator =[];
			for (var q = 0; q < coordinator_base.length; q++) {
					coordinator[q] = coordinator_base[q].id;
			}

		var registered_user = user_data.itinerary.data[i].register_user;
          if(registered_user!=null){
             var reg_base = registered_user.split(','); 
          }else{
              var reg_base = [];
          }
if((inArray(u_role, role_base) || inArray(u_base, user_base) || inArray(u_base, coordinator))){
	  	itinerary_view += "<div><div class='wrap food' style='background-image: url("+user_data.itinerary.data[i].dashboard_image+")'><div class='slide-overlay'></div><div class='item-wrapper'><div class='lunch'><p class='elaborate'>";
	  	if(user_data.itinerary.data[i].registration == 1){
	  		itinerary_view += "<span class='results'>Register</span>";
	  	}
	  	itinerary_view += user_data.itinerary.data[i].title+"</p><br>";
	  	if(user_data.itinerary.data[i].registration == 1){
            if((inArray(u_role, role_base) || inArray(u_base, user_base))){
                
                
            
	  		if(user_data.itinerary.data[i].limited_seats_status == "1"){
                var seats_left=parseInt(user_data.itinerary.data[i].limited_seats)-parseInt(user_data.itinerary.data[i].registration_status.total_registered);
                var seats_left_data;
                if(seats_left>1)
                {seats_left_data=seats_left+" Seats Left";
                }else if(seats_left==0){
                    seats_left_data='Registration Seat Over.'
                }else{
                    seats_left_data=seats_left+" Seat Left";
                }
                
	  			if(inArray(u_id, reg_base)){
	  				itinerary_view += "<p class='seats-left'>Already Registered.</p>";
	  				}else{
	  					itinerary_view += "<p class='seats-left'>"+seats_left_data+"</p>";
	  				}
		  		

	  			}else{

	  				if(inArray(u_id, reg_base)){
	  				itinerary_view += "<p class='seats-left'>Already Registered.</p>";
	  				}
	  			}
        var r_expiye_date=formatDate_last_regs(user_data.itinerary.data[i].registration_expiry_date,2);
	  	itinerary_view += "<br><p class='date'>Register Till : "+r_expiye_date+"</p>";
	  	    }else{
                
                
                if(user_data.itinerary.data[i].limited_seats_status == "1"){
                var seats_left=parseInt(user_data.itinerary.data[i].limited_seats)-parseInt(user_data.itinerary.data[i].registration_status.total_registered);
                var seats_left_data;
                if(seats_left>1)
                {seats_left_data=seats_left+" Seats Left";
                }else if(seats_left==0){
                    seats_left_data='Registration Full.'
                }else{
                    seats_left_data=seats_left+" Seat Left";
                }
	  			
                itinerary_view += "<p class='seats-left'>"+seats_left_data+"</p>";
		  		

	  			}else{

	  				if(inArray(u_id, reg_base)){
	  				itinerary_view += "<p class='seats-left'>Already Registered.</p>";
	  				}
	  			}
                
        var r_expiye_date=formatDate_last_regs(user_data.itinerary.data[i].registration_expiry_date,2);
	  	itinerary_view += "<br><p class='date'>Registration Till: "+r_expiye_date+"</p>";
                
            }
        }
	  	itinerary_view += "</div><div class='bottom-items'><p>";
    var iti_start_d_t=formatDate_last_regs(user_data.itinerary.data[i].start_date_time,1);
	  	if(user_data.itinerary.data[i].itinerary_id == 1){
	  		if(user_data.itinerary.data[i].content.transport_type == "Other"){
	  			itinerary_view += user_data.itinerary.data[i].content.other_type;
	  		}else{
	  			itinerary_view += user_data.itinerary.data[i].content.transport_type;
	  		}
	  		itinerary_view += "</p><br><p>"+iti_start_d_t+"</p><br><p>"+user_data.itinerary.data[i].content.transport_start_location+"</p>";

	  	}else if(user_data.itinerary.data[i].itinerary_id == 2){
	  		itinerary_view += user_data.itinerary.data[i].content.type_of_meal;
	  		itinerary_view += "</p><br><p>"+iti_start_d_t+"</p><br><p>"+user_data.itinerary.data[i].content.meal_location+"</p>";
	  	}else{}
	  	

	  	itinerary_view += "</div><div class='buttons'>";

	  	if(inArray(u_id, coordinator)){
	  	itinerary_view += "<button class='view-reg coordinator_view' data-company='"+user_data.user.company_id+"' id='"+user_data.itinerary.data[i].id+"'>View Registrations</button>";
	  	}

	  	itinerary_view += "<button class='tell-me' id='"+user_data.itinerary.data[i].id+"'>Tell Me More</button>";

	  	itinerary_view += "</div></div><div class='slide-navigation lunch-navigation' id='tell_me"+user_data.itinerary.data[i].id+"'><span class='cancel' id='"+user_data.itinerary.data[i].id+"'><i class='fa fa-times' aria-hidden='true'></i></span>";

	  	if(user_data.itinerary.data[i].registration == 1){
            if((inArray(u_role, role_base) || inArray(u_base, user_base))){
	  		if(inArray(u_id, reg_base)){
	  				itinerary_view += "<div class='register' id='"+user_data.itinerary.data[i].id+"yes_reg'><p>Thank You for Registration.</p><button class='btn_action register-btn' data-yesno='0' data-user='"+u_id+"' data-company='"+c_id+"' data-status='0' id='"+user_data.itinerary.data[i].id+"'>UnRegister</button></div><div class='register' style='display: none;' id='"+user_data.itinerary.data[i].id+"no_reg'><p>You have UnRegistered.</p><button class='btn_action register-btn' data-yesno='0' data-user='"+u_id+"' data-company='"+c_id+"' data-status='1' id='"+user_data.itinerary.data[i].id+"'>Register</button></div>";
	  								 }else{

	  				var exp_date = user_data.itinerary.data[i].registration_expiry_date.split(" ");
	  				var exp_date1 =  exp_date[0].split("-");
	  				var final_exp_date = exp_date1[1]+"/"+exp_date1[0]+"/"+exp_date1[2]+" "+exp_date[1]+" "+exp_date[2];
	  				
   if(new Date() <= new Date (final_exp_date)){
                 if(parseInt(user_data.itinerary.data[i].limited_seats)===parseInt(user_data.itinerary.data[i].registration_status.total_registered)){
                            itinerary_view +="<br><br>"    
                            }else{
	  				                 itinerary_view += "<div class='register' id='"+user_data.itinerary.data[i].id+"reg_yes_no'><p>Are You Attending ?</p><button class='yes-btn btn_action' data-yesno='1' data-user='"+u_id+"' data-company='"+c_id+"' data-status='1' id='"+user_data.itinerary.data[i].id+"'>Yes</button> <button class='yes-btn btn_action' data-yesno='1' data-user='"+u_id+"' data-company='"+c_id+"' data-status='0' id='"+user_data.itinerary.data[i].id+"'>No</button></div><div class='register' style='display: none;' id='"+user_data.itinerary.data[i].id+"yes_reg'><p>Thank You for Registration.</p><button class='btn_action register-btn' data-yesno='1' data-user='"+u_id+"' data-company='"+c_id+"' data-status='0' id='"+user_data.itinerary.data[i].id+"'>UnRegister</button></div><div class='register' style='display: none;' id='"+user_data.itinerary.data[i].id+"no_reg'><p>You have UnRegistered.</p><button class='btn_action register-btn' data-yesno='1' data-user='"+u_id+"' data-company='"+c_id+"' data-status='1' id='"+user_data.itinerary.data[i].id+"'>Register</button></div>";
	  					            }
                                            
                            }else{
	  					itinerary_view += "<div class='register'><p>Registration Expired!</p></div>";
	  											}

	  								 }

	  	

	  	//itinerary_view += "</span></div><div class='details details-b'><p class='data'>Details</p><ul><p>Onbording</p><li>Documents required during checkin - PhotoID proof (PAN, Aadhar card, Passport)</li></ul><ul><p>Facilities</p><li>2 Meal (Veg)</li><li>Beverages (Water, Juice)0</li><li>First Aid kit</li><li>Fun activities during travel</li></ul></div>";
        }else{
            itinerary_view +="<br><br>"
        }
            
        if(user_data.itinerary.data[i].additional_attachment !=""){
	  		itinerary_view += "<div class='attachement' id ='"+user_data.itinerary.data[i].id+"attachement'><i class='fa fa-paperclip' aria-hidden='true'></i> <span><a href='"+user_data.itinerary.data[i].additional_attachment+"' target='_blank'> View Attachement</a></span></div>";
	  	}else{
	  		//document.getElementById(user_data.itinerary.data[i].id+'attachement').style.display = "none";
	  	}
            
         if(user_data.itinerary.data[i].additional_information!=''){
               itinerary_view += "<div class='details details-b'><p class='data'>Registration Details</p>"+user_data.itinerary.data[i].additional_information+"</div>";
               }
	  	
    
        
        
        }
    
	  	var sd = user_data.itinerary.data[i].start_date_time.split(' ');
	  	var ed = user_data.itinerary.data[i].end_date_time.split(' ');
	  	if(user_data.itinerary.data[i].itinerary_id == 1){
	  	itinerary_view += "<div class='time'><div class='time-a col'><p>"+sd[1]+" "+sd[2]+"</p><p>Start</p></div><div class='time-b col'><p>"+ed[1]+" "+ed[2]+"</p><p>End</p></div></div>";
	  	}else if(user_data.itinerary.data[i].itinerary_id == 2){
	  	itinerary_view += "<div class='time'><div class='time-a col'><p>"+sd[1]+" "+sd[2]+"</p><p>Start</p></div><div class='time-b col'><p>"+ed[1]+" "+ed[2]+"</p><p>End</p></div></div>";
	  			}else{}
	  	//itinerary_view += "<div class='time distance'><div class='time-a col'><p>350 km</p><p>Distance</p></div><div class='time-b col'><p>4h 30m</p><p>Travel Time</p></div></div>";

	  	itinerary_view += "<div class='attachement bus-ticket'><i class='fa fa-paperclip clip-2' aria-hidden='true'></i><span>";
       var transport_details='';
	  	if(user_data.itinerary.data[i].itinerary_id == 1){
	  		if(user_data.itinerary.data[i].content.transport_ticket !=""){
	  			itinerary_view += "<a href='"+user_data.itinerary.data[i].content.transport_ticket+"' target='_blank'> View Attachement</a>";
	  		}else{
	  			itinerary_view += " No Attachement";
	  		}
            transport_details="<p class='data'>Details</p>";
	  		
	  	}else if(user_data.itinerary.data[i].itinerary_id == 2){
	  		if(user_data.itinerary.data[i].content.menu_option  !=""){
	  			itinerary_view += "<a href='"+user_data.itinerary.data[i].content.menu_option+"' target='_blank'> View Attachement</a>";
	  		}else{
	  			itinerary_view += " No Attachement";
	  		}
	  		
	  	}else{}
        
	  	itinerary_view += "</span></div><div class='details'>"+transport_details+""+user_data.itinerary.data[i].content.information+"</div></div><div class='register-navigation lunch-register'><div class='close-register'><i class='fa fa-times' aria-hidden='true'></i></div><div class='row'><div class='col-xl-4 col box-a'><span>Seats</span><span id='r_seat' class='"+user_data.itinerary.data[i].id+"r_seat'></span></div><div class='col-xl-4 col box-b'><span>Registered</span><span id='r_register' class='"+user_data.itinerary.data[i].id+"r_register' ></span></div><div class='col-xl-4 col box-c'><span>Available</span><span id='r_available' class='"+user_data.itinerary.data[i].id+"r_available'></span></div></div><div class='register-tabs'><a class='register-button' href='#'>Registered</a><a class='available-button' href='#'>Available</a></div><div id='cordinator_view' class='"+user_data.itinerary.data[i].id+"cordinator_view'></div></div></div></div>";
    
           }
	  }
    
	}
    else{itinerary_view += "<div>No Itinerary Available.</div>";}
	  //console.log(itinerary_view);
      console.log("itinerary check");
      console.log(itinerary_view);
      if(itinerary_view==''){
         itinerary_view += "<div>No Itinerary Available.</div>";
         }
      console.log(itinerary_view);
	  document.getElementById("itinerarylist").innerHTML = itinerary_view;

	  $('.coordinator_view').on("click",function(){
	    var i_id =  $(this).attr("id");
	    var c_id =  $(this).data("company");
	    	    
	    //alert(i_id);
	    //alert(c_id);

    	var data_type={"company_id": c_id, "itinerary_id": i_id};
		console.log(data_type);
		$.ajax({  

		url: localStorage.getItem("base_url")+'itinerary_registration_status',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(data_type),
		ContentType: 'application/json',
		type : 'POST',
		dataType : 'json',
		async: false,
		success : function (output) {
			if (output.status==1000){
				console.log("Cordinator Data");
				console.log(output);
				var cordinator_data = JSON.parse(JSON.stringify(output.data));
                
                $('.'+i_id+'r_seat').text(cordinator_data.registration_status.limited_seats);
                $('.'+i_id+'r_register').text(cordinator_data.registration_status.total_resgistered);
                $('.'+i_id+'r_available').text(cordinator_data.registration_status.total_available);

				
				var cordinator_view = "<div class='register-list registered'><ul>";
				if(cordinator_data.registration_status.total_resgistered){
					for (var x = 0; x < cordinator_data.registration_status.total_resgistered; x++) {
						cordinator_view += "<li><img src='";
						if(cordinator_data.registration_status.register_user[x].avatar){
							cordinator_view += cordinator_data.registration_status.register_user[x].avatar;
						}else{
							cordinator_view += "assets/pic.png";
						}
						cordinator_view += "'> <span>"+cordinator_data.registration_status.register_user[x].name+"</span></li>";
					}
				}else{cordinator_view += "<li>No Data Available</li>";}
				
				cordinator_view += "</ul></div><div class='register-list available'><ul>";

				if(cordinator_data.registration_status.total_available){
					for (var y = 0; y < cordinator_data.registration_status.total_available; y++) {
						cordinator_view += "<li><img src='";
						if(cordinator_data.registration_status.available_user[y].avatar){
							cordinator_view += cordinator_data.registration_status.available_user[y].avatar;
						}else{
							cordinator_view += "assets/pic.png";
						}
						cordinator_view += "'> <span>"+cordinator_data.registration_status.available_user[y].name+"</span></li>";
					}
				}else{cordinator_view += "<li>No Data Available</li>";}

				cordinator_view += "</ul></div>";

                 console.log(i_id+"cordinator_view");
                console.log('.'+i_id+'cordinator_view');
                $('.'+i_id+'cordinator_view').html(cordinator_view); 
				//document.getElementById("cordinator_view").innerHTML = cordinator_view;


			}else{
				console.log(output.message);
			}
		},
		error : function(){
			alert("Error in loading records. Please refresh the page and try again.");
		}
		}); 
	});

	function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
	}


	$(".btn_action").click(function(){
  	var i_id =  $(this).attr("id");
  	var c_id =  $(this).data("company");
  	var u_id =  $(this).data("user");
  	var reg_status =  $(this).data("status");
  	var yesno =  $(this).data("yesno");

  	var data_type={"company_id": c_id,"itinerary_id": i_id,"user_id": u_id,"registration_status": reg_status};
	console.log(data_type);
	$.ajax({  

		url: localStorage.getItem("base_url")+'itinerary_registration',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(data_type),
		ContentType: 'application/json',
		type : 'POST',
		dataType : 'json',
		async: false,
		success : function (output) {
			if (output.status==1000){
				console.log(output.message);

				if(reg_status == 0){
									if(yesno == 1){document.getElementById(i_id+"reg_yes_no").style.display = "none";}
									document.getElementById(i_id+"yes_reg").style.display = "none";
									document.getElementById(i_id+"no_reg").style.display = "block";
									}else{
									if(yesno == 1){document.getElementById(i_id+"reg_yes_no").style.display = "none";}
									document.getElementById(i_id+"no_reg").style.display = "none";
									document.getElementById(i_id+"yes_reg").style.display = "block";
										}
					if(yesno == 1 && reg_status == 0){}else{window.location.reload();}

			}else{
                alert(output.message)
				console.log(output.message);
			}
		},
		error : function(){
			alert("Error in loading records. Please refresh the page and try again.");
		}
	});
 
  	
  });

  $(".tell-me").click(function(){
  	var tell_me_id =  $(this).attr("id");
  	$("#tell_me"+tell_me_id).css({
                          "position":"absolute",
                          "right":"0px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  $(".cancel").click(function(){
  	var tell_me_id =  $(this).attr("id");
  	$("#tell_me"+tell_me_id).css({
                          "position":"absolute",
                          "right":"-370px",
                          "transition":"0.3s ease-in-out",
  	});
  });

    });


	//JQuery for Product Slide Show//
$(document).ready(function(){    
  $('.products').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    dots: true,
    arrows: true,
    autoplaySpeed: 3000,
    responsive: [
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        infinite: true,
        dots: true,
        arrows: false
      }
    },
    
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });

  $(".available-button").click(function(){
    $(".available").fadeIn('slow');
    $(".registered").hide();
  });
  $(".register-button").click(function(){
    $(".available").hide();
    $(".registered").fadeIn('slow');
  });

  $(".view-reg").click(function(){
  	$(".lunch-register").css({
                          "position":"absolute",
                          "right":"0px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  $(".close-register").click(function(){
  	$(".lunch-register").css({
                          "position":"absolute",
                          "right":"-370px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  $(".view-reg-travel").click(function(){
  	$(".travel-register").css({
                          "position":"absolute",
                          "right":"0px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  $(".close-register").click(function(){
  	$(".travel-register").css({
                          "position":"absolute",
                          "right":"-370px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  $(".view-reg-stay").click(function(){
  	$(".stay-register").css({
                          "position":"absolute",
                          "right":"0px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  $(".close-register").click(function(){
  	$(".stay-register").css({
                          "position":"absolute",
                          "right":"-370px",
                          "transition":"0.3s ease-in-out",
  	});
  });
  // $(".results").click(function(){
  // 	alert("Hello Results");
  // })
});
//JQuery for Product Slide Show//
